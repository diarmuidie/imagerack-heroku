<?php

namespace Templates;

use \Diarmuidie\ImageRack\Image\TemplateInterface;

/**
 * Sample template to fit an image to 640px x 427px
 */
class Large implements TemplateInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(\Intervention\Image\Image $image)
    {
        // Manipulate the image as required
        $image->fit(640, 427);

        // Return the manipulated image
        return $image;
    }
}
