<?php

use League\Flysystem\Filesystem;
use Aws\S3\S3Client;
use League\Flysystem\Adapter\Local;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use Intervention\Image\ImageManager;

$dependencies = [];

// Create an S3 client
$s3Client = S3Client::factory([
    'credentials' => [
        'key'    => getenv('S3_KEY'),
        'secret' => getenv('S3_SECRET'),
    ],
    'region' => 'us-east-1',
    'version' => 'latest',
]);

// The source image filesystem
$dependencies['source'] = new Filesystem(new AwsS3Adapter($s3Client, 'heroku-imagerack-source'));

// The cache image filesystem
$dependencies['cache'] = new Filesystem(new Local(__DIR__.'/../storage/cache'));

// The Intervention image manager instance
$dependencies['imageManager'] = new ImageManager(array('driver' => 'gd'));

return $dependencies;
