Sample ImageRack Heroku & S3 Deploy
===================================

Read the blog post at [diarmuid.ie/blog/setting-up-a-php-image-server-on-heroku/](http://diarmuid.ie/blog/setting-up-a-php-image-server-on-heroku/).

***

_Sample images from a live Heroku app running the code in this repo:_

### Large Image

http://imagerack.herokuapp.com/large/photo1.jpg

![Large Photo](http://imagerack.herokuapp.com/large/photo1.jpg)

### Small Image

http://imagerack.herokuapp.com/small/photo1.jpg

![Small Photo](http://imagerack.herokuapp.com/small/photo1.jpg)

### Pixelated Image

http://imagerack.herokuapp.com/pixelated/photo1.jpg

![Large Photo](http://imagerack.herokuapp.com/pixelated/photo1.jpg)

