<?php

require '../vendor/autoload.php';

// Load our dependencies
$dependencies = require_once __DIR__.'/../bootstrap/dependencies.php';

// Run the imageRack server
$server = new Diarmuidie\ImageRack\Server(
    $dependencies['source'],
    $dependencies['cache'],
    $dependencies['imageManager']
);

// Set each available template with a callback to return a processor
// that implements Diarmuidie\ImageRack\Image\TemplateInterface
$server->setTemplate('small', function () {
    return new Templates\Small();
});

$server->setTemplate('large', function () {
    return new Templates\Large();
});

$server->setTemplate('pixelated', function () {
    return new Templates\Pixelated();
});

// Uncomment to edit the default cache http header max age (in seconds).
// Set to zero to disable browser caching
// $server->setHttpCacheMaxAge(86400);

// Uncomment to edit the default not found response
// $server->setNotFound(function ($response) {
//     return $response->setContent('Image not found.');
// });

// Uncomment to edit the default error response
// $server->setError(function ($response, $exception) {
//     return $response->setContent('An internal error occurred. ' . $exception->getMessage());
// });

// Run the server for this request
$server->run();

// Send the response to the browser
$server->send();
