<?php
/**
 * Script to force clear the cache folder.
 * For testing only don't deploy oin production.
 */

require '../vendor/autoload.php';

$dependencies = require_once __DIR__.'/../bootstrap/dependencies.php';

$cache = $dependencies['cache'];

$small = $cache->deleteDir('small');
$large = $cache->deleteDir('large');
$pixelated = $cache->deleteDir('pixelated');

echo "Cache directories cleared";
