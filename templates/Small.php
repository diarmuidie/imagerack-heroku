<?php

namespace Templates;

use \Diarmuidie\ImageRack\Image\TemplateInterface;

/**
 * Sample template to fit an image to 300px x 200px
 */
class Small implements TemplateInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(\Intervention\Image\Image $image)
    {
        // Manipulate the image as required
        $image->fit(300, 200);

        // Return the manipulated image
        return $image;
    }
}
